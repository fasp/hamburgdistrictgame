import os
from flask import Flask, Response, render_template, request, jsonify, make_response, json
from flask import send_from_directory

app = Flask(__name__, static_url_path='')

@app.route('/')
def serve_page():
    return send_from_directory('web', 'index.html')
    #return render_template('web/index.html')

@app.route('/app.js')
def serve_element(element):
    return send_from_directory('web', 'app.js')

@app.route('/<name>')
def my_view_func(name):
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    if name == 'hamburg':
        json_url = os.path.join(SITE_ROOT, "static", "de_hh_up_regionalstatistische_daten_stadtteile_EPSG_4326.json")
        return read_and_return(json_url)
    elif name == 'hh_min':
        json_url = os.path.join(SITE_ROOT, "static", "de_hh_2022.json")
        return read_and_return(json_url)
    elif name == 'world':
        json_url = os.path.join(SITE_ROOT, "static", "world_map_web_merc.json")
        return read_and_return(json_url)

def read_and_return(url):
    with open(url) as f:
        lines = f.readlines()
        resp = Response(response=lines,
            status=200,
            mimetype="application/json")
        resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp

if __name__ == "__main__":
  # app.config["TEMPLATES_AUTO_RELOAD"] = True
  app.run(debug=True)
