import os
from flask import Flask, Response, render_template, request, jsonify, make_response, json, url_for

def taiwan():
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url = os.path.join(SITE_ROOT, "static", "de_hh_up_regionalstatistische_daten_stadtteile_EPSG_4326.json")
    data = json.load(open(json_url))
    all_features = data['features']
    #[print(ele['properties']['jahr']) for ele in all_features]
    
    filtered = [x for x in all_features if x['properties']['jahr'] == '2022']
    #[print(ele['properties']['jahr']) for ele in filtered]

    data['features']=filtered
    #json.jsonify(data)
    with open('static/de_hh_2022.json', 'w') as f:
        json.dump(data, f)
    #return render_template('taiwan.jade', data=data)


if __name__ == "__main__":
  taiwan()
