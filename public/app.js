var map;
var districts_list = [];
var district_properties = [];
var currentDistrictIdx = 0;
var lastPolygon;

addMap();
addHamburg();

function addMap() {
   var mapOptions = {
      center: [53.56722407506542, 10.027703828424894],
      zoom: 11
   }

   map = new L.map('mymap', mapOptions);
   //map.removeLayer('label');

   // See https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
   var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

   map.addLayer(layer);
}

function addHamburg() {
   fetch("de_hh_2022.json").then((response) => {
      return response.json();
   }).then((topology) => {
      districts_list = topology.features;
      drawAllDistricts();
      setQuiz();
   });
}

function drawAllDistricts() {
   for (i = 0; i < districts_list.length; i++) {
      drawDistrict(i, 'red');
   }
}

function drawDistrict(districtIdx, color) {
   var district = districts_list[districtIdx];
   district_properties[districtIdx] = district.properties;
   var district_geo = district.geometry;
   var swapped_coords = swapXandY(district_geo.coordinates[0]);
   lastPolygon = L.polygon(swapped_coords, { color: color, id: i }).addTo(map);
   lastPolygon.on('click', function (e) {
      var resultText = currentDistrictIdx == districtIdx ? "Correct!" : "Wrong!";
      var popupClass = currentDistrictIdx == districtIdx ? 'correct-popup' : 'incorrect-popup';
      var properties = district_properties[districtIdx];
      var popLocation = e.latlng;
      var popup = L.popup({ className: popupClass })
         .setLatLng(popLocation)
         .setContent('<b>' + resultText + '</b><br>' + properties.stadtteil + '</p>');
      popup.openOn(map);
   });
}

function swapXandY(coords) {
   var swapped_coords = [];
   coords.forEach(element => {
      swapped_coords.push(element.map(coord => [coord[1], coord[0]]));
   });
   return swapped_coords;
}

function setQuiz() {
   currentDistrictIdx = Math.floor(Math.random() * district_properties.length);
   var district = district_properties[currentDistrictIdx].stadtteil;
   document.getElementById("quizpanel").innerHTML = "<h2><center>Where is " + district + "?</center>";
   map.removeLayer(lastPolygon);
   map.closePopup();
}

function showDistrict() {
   map.removeLayer(lastPolygon);
   drawDistrict(currentDistrictIdx, 'blue');
   map.closePopup();
   // var district = districts_list[districtIdx];
   // var coords = district.geometry.coordinates[0];
   // var swapped_coords = swapXandY(coords);
   // var polygon = L.polygon(swapped_coords, { color: 'blue'}).addTo(map);
   //map.fitBounds(polygon.getBounds());
}