# HamburgDistrictGame

## Useful links
- https://opendatalab.de/projects/geojson-utilities/
- https://metaver.de/trefferanzeige?docuuid=F4062BD8-43C4-4C4F-AA45-253D84A3685E#detail_links
- https://hub.arcgis.com/datasets/esri-de-content::stadtteile-hamburg/explore?location=53.532741%2C9.954943%2C11.00
- https://hub.arcgis.com/datasets/esri-de-content::stadtteile-hamburg/about
- https://mapshaper.org/
- https://www.naturalearthdata.com/downloads/110m-cultural-vectors/
- https://github.com/Melonman0/InteractiveMap?tab=readme-ov-file
- https://metaver.de/trefferanzeige?docuuid=F4062BD8-43C4-4C4F-AA45-253D84A3685E
